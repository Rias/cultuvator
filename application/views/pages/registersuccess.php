<?php echo doctype('html5') ?><!--[if IE 8]>                 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
    <!--<![endif]-->
    <head>
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <title>
            Cultuvator
        </title>
        <link rel="stylesheet" href="/assets/css/normalize.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/fontello.css">
    </head>
    <body style="overflow: scroll;-webkit-overflow-scrolling: touch;-moz-overflow-scrolling: touch;">
        <div class="relative tall">
            <div id="container" class="absolute">
                <div class="relative tall">
                    <div id="content">
                        <section id="header">
                            <div id="slogan">
                                <p>
                                    Cultuvator
                                </p>
                                <p class="icon-location"></p>
                                <p>
                                    Share your culture activity.
                                </p>
                            </div>
                        </section>
                        <section id="main_content" class="scrollable">
                            <div class="article">
                                <?php
                                  echo anchor('login', 'Inloggen', array('name' => 'login', 'value' => 'Inloggen', 'class' => 'linkbutton'));
                                  echo "<p>Registratie gelukt!</p>";
                                ?>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div><script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
</script><script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1EiLMvQPfwTRpR06fQhGwi1B4TjuKbFE&amp;sensor=true">
</script><script type="text/javascript" src="/assets/js/location-ck.js">
</script><script type='text/javascript' src='/assets/js/waypoints-0.0.1.js' ></script>
<script type="text/javascript">
          Waypoints
                  .intercept('a')
                  .ignore('.external')
        </script>
    </body>
</html>