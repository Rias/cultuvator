        <div id="container" class="absolute">
            <div id="toolbar" class="fixed">
                <span>Cultuvator <a href="" id="back" class="left"></a></span>
            </div>
            <div id="content">
                <section id="main_content" class="scrollable">
                    <div class="article socialButtons">
                      <div class="social facebookbutton active">
                        <i class="icon-facebook"></i>
                      </div>
                      <div class="social twitterbutton">
                        <i class="icon-twitter"></i>
                      </div>
                      <div class="social gplusbutton">
                        <i class="icon-gplus"></i>
                      </div>
                    </div>
                    <div class="article facebook">

                        <!-- Facebook button -->
                        <?php echo '<p>Je zal zelf nog je bericht moeten typen; De titel van het evenement is: "'. $title . '"</p>'; ?><a href="https://www.facebook.com/dialog/feed?app_id=451652708258178&%20link=http://cultuvator.webduivel.be/home&%20picture=http://cultuvator.webduivel.be/assets/img/logo/512.png&%20name=Cultuvator&%20caption=Share%20your%20culture%20activity&%20redirect_uri=http://cultuvator.webduivel.be/share?title=<?php echo $title ?>" class="linkbutton share external">Deel</a>
                    </div>
                    <div class="article twitter">
                        <p>
                            Deel je checkin op Twitter!
                        </p>

                        <?php $bericht = "Ik heb me zojuist ingecheckt bij $title via"; ?>

                        <!-- Tweet button -->
                        <a href="http://twitter.com/share?text=<?php echo $bericht; ?>&url=&related=riasvdv&hashtags=Cultuvator" class="linkbutton share external">Tweet</a>
                    </div>
                    <div class="article gplus">
                        <p>
                            Deel je checkin op Google+!
                        </p>

                        <!-- Google plus button -->
                        <div class="g-interactivepost linkbutton share" data-contenturl="http://cultuvator.webduivel.be" data-contentdeeplinkid="/pages" data-clientid="293771151529.apps.googleusercontent.com" data-cookiepolicy="http://webduivel.be" data-prefilltext="Ik heb me zojuist ingecheckt bij <?php echo $title; ?> via #Cultuvator" data-calltoactionlabel="JOIN" data-calltoactionurl="http://cultuvator.webduivel.be" data-calltoactiondeeplinkid="/home">
                            Post
                        </div>
                    </div>
                    <div class="article">
                        <p>
                            Als je klaar bent met alles te delen kan je terugkeren naar de homepage.
                        </p><a class="linkbutton share home" href="home">Home</a>
                    </div>
                </section>
            </div>
        </div><script type="text/javascript" src="http://platform.twitter.com/widgets.js">
</script><script type="text/javascript">
(function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/client:plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
        </script>
