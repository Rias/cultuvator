<div id="container" class="absolute">
    <div id="toolbar" class="fixed">
        <span>Cultuvator
          <a href="" id="back" class="left"><i class="icon-arrow-left"></i></a>
        </span>
    </div>
    <div id="content">
      <section id="main_content" class="scrollable">
        <div class="article checkinInfo">
          <?php
            if($checkin == "success"){
              echo '<p>Ingecheckt bij: "'  . $eventTitle . '"</p>';
            }
            else
            {
              echo "Er is iets misgelopen, probeer opnieuw. Error: ". $checkin;
            }
           ?>
        </div>

        <div class="article badgeInfo">
            <?php

            //Badge tonen
            $baseurl = base_url();
            if($badgeFound){
                echo "<span class='badge'><img src='".$baseurl."assets/img/badges/".$catname."_".$badgeIconUrl.".png' /></span>";
                echo 'Huidige titel: "' . $badgeTitle. '"';
            }else
            {
              echo "<span class='badge'><img src='".$baseurl."assets/img/badges/no_badge.png' /></span>";
            }

            //Berekening van nodig aantal checkins voor volgende badge
            if($neededPoints > 0){
              $currentPoints = $totalPoints - $neededPoints;
              $normalisatie = $totalPoints - 25;
              $breedte = (($currentPoints - $normalisatie)/($totalPoints-$normalisatie))*100;
              ?>
                <div class="loading-container">
                  <div class="loading-progress" style="width: <?php echo $breedte; ?>%;"></div>
                </div>
              <?php
              echo "<p>Nog ". $neededPoints . " checkin";
              if($neededPoints > 1){
                echo "s";
              }
              echo" nodig in deze categorie tot de volgende badge!</p>";
            }
            ?>

        </div>
        <div class="buttons">
          <?php
            echo anchor('home', 'Doorgaan', array('class' => 'linkbutton checkin'));
           ?>
           <a href="share?title=<?php echo $eventTitle;?>" class="linkbutton checkin">Deel checkin</a>
        </div>
      </section>
    </div>
</div>
</div>
