<div id="container" class="absolute">
    <div id="toolbar" class="fixed">
        <span>Cultuvator
          <a href="#" id="open-left" class="left"><i class="icon-menu"></i></a><a href="logout" id="logout" class="right">Logout</a>
        </span>
    </div>
    <div id="content">
      <div id="map-canvas">
        <!-- Google maps kaart -->
      </div>
      <section id="main_content" class="scrollable">
      <?php
        //zoekformulier
        echo form_open('listevents', array('id' => 'search_form'));

        echo form_input(array('name' => 'q', 'placeholder' => 'Activiteit niet in de lijst? Zoek even...'));
        echo form_submit(array('name' => 'search', 'class' => 'button', 'value' => 'Zoek'));

        echo form_close();

        //evenementlijst
        if (!empty($jsondata)){
          foreach($jsondata as $event){
            $img = explode(';', $event['headingid']);
           ?>
           <div class="article">
            <!-- Check-in knop -->
            <a href="checkin?eid=<?php echo $event['cdbid']; ?>&cid=<?php echo $img[0]; ?>&title=<?php echo $event['title'] ?>" class="linkbutton">Check-in</a>

            <!-- Categorie afbeelding -->
            <a href="eventdetail?id=<?php echo $event['cdbid']; ?>"><i class="image <?php

              //Kijken welke categorie
              switch ($img[0]) {
                case $img[0] >= 1 && $img[0] < 5:
                  echo "icon-evenementen";
                  break;
                case 5:
                  echo "icon-films";
                  break;
                case $img[0] >=7 && $img[0] < 14:
                  echo "icon-expo";
                  break;
                case $img[0] >=14 && $img[0] < 22:
                  echo "icon-muziek";
                  break;
                case $img[0] >=22 && $img[0] < 25:
                  echo "icon-podium";
                  break;
                case $img[0] >=25 && $img[0] < 31:
                  echo "icon-cursussen";
                  break;
                case 31:
                  echo "icon-sport";
                  break;
                case 32:
                  echo "icon-uitstappen";
                  break;
                case 33:
                  echo "icon-uitgaan";
                  break;
                case 34:
                  echo "icon-uit";
                  break;
                case 35:
                  echo "icon-varia";
                  break;
                default:
                  echo "icon-varia";
                  break;

              }

             ?>"></i></a>
            <span class="title"><a href="eventdetail?id=<?php echo $event['cdbid']; ?>"><?php echo $event['title']; ?></a></span>

           </div>
         <?php } ?>
        <?php }else{ ?>
          <div class="article">
              <span class="title">Er zijn geen lopende evenementen gevonden.</span>
           </div>
        <?php } ?>
      </section>
    </div>
</div>
</div>
