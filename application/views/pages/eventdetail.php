<div id="container" class="absolute">
    <div id="toolbar" class="fixed">
        <span>Cultuvator
          <a href="#back" id="back" class="left"><i class="icon-arrow-left"></i></a>
        </span>
    </div>
    <div id="content">
      <section id="main_content">

        <div class="article eventdetail" class="scrollable">
          <div id="header">
            <span class="title"><?php
            //Titel tonen als deze bestaat
            if(isset($event->event->eventdetails->eventdetail->title)){echo $event->event->eventdetails->eventdetail->title;} ?></span>
            <?php

              //Afbeelding tonen als deze bestaat
              $images = "";
              if(isset($event->event->eventdetails->eventdetail->media->file)){
                $images = $event->event->eventdetails->eventdetail->media->file;
              }
              if(!is_object($images) && !empty($images))
              {
                foreach ($images as $image) {
                  if($image->mediatype == "photo")
                  {
                    echo "<img class='img' src='".$image->hlink."'>";
                  }
                }
              }
            ?>

          </div>
          <div id="eventContent">

            <!-- Evenementbeschrijving tonen als deze bestaat -->
            <p><?php if(isset($event->event->eventdetails->eventdetail->shortdescription)){echo $event->event->eventdetails->eventdetail->shortdescription;}else{echo "Geen evenementbeschrijving gevonden.";} ?></p>
              <dl>
                <dt>Wanneer</dt>
                <dd>
                  <p><?php if(isset($event->event->eventdetails->eventdetail->calendarsummary)){echo $event->event->eventdetails->eventdetail->calendarsummary;}else{echo "Geen kalenderinfo.";} ?></p>
                </dd>

                <dt>Waar</dt>
                <dd>
                  <p><?php
                  //Adresinfo tonen als dze bestaat
                  if(!empty($event->event->contactinfo->address->physical->street)){echo $event->event->contactinfo->address->physical->street;} if(!empty($event->event->contactinfo->address->physical->housenr)){echo " ". $event->event->contactinfo->address->physical->housenr ."";} ?></p>
                  <p><?php if(!empty($event->event->contactinfo->address->physical->zipcode)){echo $event->event->contactinfo->address->physical->zipcode;} if(!empty($event->event->contactinfo->address->physical->city)){echo " ". $event->event->contactinfo->address->physical->city ."";} ?></p>
                </dd>

                <dt>Prijs</dt>
                <dd>
                  <p><?php if(!empty($event->event->eventdetails->eventdetail->price->pricevalue)){echo "&euro;" . $event->event->eventdetails->eventdetail->price->pricevalue;}else{echo "n.v.t.";}?></p>
                  <p><?php if(!empty($event->event->eventdetails->eventdetail->price->pricedescription)){echo $event->event->eventdetails->eventdetail->price->pricedescription;}?></p>
                </dd>
            </dl>
          </div>
          <?php if(!empty($event->event->eventdetails->eventdetail->longdescription)){ ?>
            <a href="#" id="moreInfo" class="linkbutton">Meer info<a/>
            <div id="eventMoreInfo">
              <?php echo $event->event->eventdetails->eventdetail->longdescription; ?>
            </div>
          <?php } ?>
        </div>
      </section>
    </div>
</div>
</div>
