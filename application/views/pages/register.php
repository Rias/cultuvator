<?php echo doctype('html5') ?>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
  <meta http-equiv="x-ua-compatible" content="IE=edge" />
  <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <title>Cultuvator</title>

  <link rel="stylesheet" href="/assets/css/normalize.css" />
  <link rel="stylesheet" type="text/css" href="/assets/css/style.css" />
  <link rel="stylesheet" type="text/css" href="/assets/css/fontello.css"/>


</head>
<body style="overflow: scroll;-webkit-overflow-scrolling: touch;-moz-overflow-scrolling: touch;">
  <div class="relative tall">
<div id="container" class="absolute">
        <div class="relative tall">
            <div id="content">
              <section id="header">
                <div id="slogan">
                  <p>Cultuvator</p>
                  <p class="icon-location"></p>
                  <p>Share your culture activity.</p>
                </div>
                <?php
                  //Registratieformulier tonen
                  echo form_open('user/register', array('id' => 'register_form'));

                  echo form_error('username');
                  echo form_input(array('name' => 'username', 'placeholder' => 'Gebruikersnaam', 'value' => set_value('username')));

                  echo form_error('name');
                  echo form_input(array('name' => 'name', 'placeholder' => 'Voornaam', 'value' => set_value('name')));

                  echo form_error('surname');
                  echo form_input(array('name' => 'surname', 'placeholder' => 'Achternaam', 'value' => set_value('surname')));

                  echo form_error('email');
                  echo form_input(array('name' => 'email', 'placeholder' => 'E-mail', 'value' => set_value('email')));

                  echo form_error('password');
                  echo form_password(array('name' => 'password', 'placeholder' => 'Wachtwoord'));

                  echo form_error('password2');
                  echo form_password(array('name' => 'password2', 'placeholder' => 'Herhaal wachtwoord'));
                  echo form_submit(array('name' => 'register', 'class' => 'button', 'value' => 'Registreer'));

                  echo form_close();
                ?>
              </section>
            </div>
        </div>
    </div>
  </div>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1EiLMvQPfwTRpR06fQhGwi1B4TjuKbFE&sensor=true"></script>
  <script type="text/javascript" src="/assets/js/location-ck.js"></script>
</body>
</html>
