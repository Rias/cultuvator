<div id="container" class="absolute">
    <div id="toolbar" class="fixed">
        <span>Cultuvator
          <a href="#" id="open-left" class="left"><i class="icon-menu"></i></a>
          <a href="logout" id="logout" class="right">Logout</a>
        </span>
    </div>
    <div id="content">
      <div id="map-canvas">
        <!-- Google maps kaart -->
      </div>
      <section id="main_content" class="scrollable">
          <?php

              //Welkomsbericht met Naam en Achternaam
              echo "<div class='article'>Welkom ". $userFullName->name . " ". $userFullName->surname  ."!</div>";


              //Knop om in te checken
              echo anchor('listevents', 'Zoek evenementen om in te checken', array('class' => 'linkbutton home'));
              //Recente checkins tonen

              echo "<p class='title'>Laatste 10 Checkins:</p>";

              if(!empty($checkins)){
                  foreach ($checkins as $checkin) {
                  ?>
                  <div class="article">

                    <a href="eventdetail?id=<?php echo $checkin['eventID']; ?>"><i class="image icon-<?php echo $checkin['category']; ?>"></i></a>
                    <span class="title"><a href="eventdetail?id=<?php echo $checkin['eventID']; ?>"><?php echo $checkin['title']; ?></a></span>

                  </div>
                  <?php
                }
              }
              else
              {
                echo "<div class='article'>Geen recente checkins gevonden.</div>";
              }
          ?>
      </section>
    </div>
</div>
</div>
