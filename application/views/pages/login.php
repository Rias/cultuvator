<?php
  if($this->session->userdata('logged_in') != null){
    redirect('home');
  }
 ?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en" itemscope="" itemtype="http://schema.org/Article">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8"><!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=edge" /><![endif]-->
        <meta name="viewport" content="initial-scale=1,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta itemprop="name" content="Cultuvator">
        <meta itemprop="description" content="Share your culture activity">
        <meta itemprop="image" content="http://cultuvator.webduivel.be/assets/img/logo/512.png">

        <!-- iPhone -->
        <link href="/assets/img/app/apple-touch-startup-image-320x460.png" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 1)" rel="apple-touch-startup-image" />
        <!-- iPhone (Retina) -->
        <link href="/assets/img/app/apple-touch-startup-image-640x920.png" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image">
        <!-- iPhone 5 -->
        <link href="/assets/img/app/apple-touch-startup-image-640x1096.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image">
        <!-- iPad (portrait) -->
        <link href="/assets/img/app/apple-touch-startup-image-768x1004.png" media="(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 1)" rel="apple-touch-startup-image">
        <!-- iPad (landscape) -->
        <link href="/assets/img/app/apple-touch-startup-image-748x1024.png" media="(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 1)" rel="apple-touch-startup-image">
        <!-- iPad (Retina, portrait) -->
        <link href="/assets/img/app/apple-touch-startup-image-1536x2008.png"  media="(device-width: 768px) and (device-height: 1024px) and (orientation: portrait) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image">
        <!-- iPad (Retina, landscape) -->
        <link href="/assets/img/app/apple-touch-startup-image-1496x2048.png" media="(device-width: 768px) and (device-height: 1024px) and (orientation: landscape) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image">

        <!-- Home screen icons -->
        <link href="/assets/img/app/Icon.png" rel="apple-touch-icon-precomposed" />
        <link href="/assets/img/app/Icon-72.png" rel="apple-touch-icon-precomposed" sizes="72x72" />
        <link href="/assets/img/app/Icon@2x.png" rel="apple-touch-icon-precomposed" sizes="114x114" />
        <link href="/assets/img/app/Icon-72@2x.png" rel="apple-touch-icon-precomposed" sizes="144x144" />


        <title>
            Cultuvator
        </title>
        <link rel="stylesheet" href="/assets/css/normalize.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/fontello.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/add2home.css">
    </head>
    <body style="overflow: scroll;-webkit-overflow-scrolling: touch;-moz-overflow-scrolling: touch;">
        <div class="relative tall">
            <div id="container" class="absolute">
                <div class="relative tall">
                    <div id="content" style="padding-top: 20px;">
                        <section id="header">
                            <div id="slogan">
                                <p>
                                    Cultuvator
                                </p>
                                <p class="icon-location"></p>
                                <p>
                                    Share your culture activity.
                                </p>
                            </div><?php
                                //Login formulier tonen.
                                echo form_open('user/login', array('id' => 'login_form'));
                                echo "<p id='error'>". validation_errors() . "</p>";
                                echo form_input(array('name' => 'username', 'placeholder' => 'Gebruikersnaam', 'autocomplete' => 'off'));
                                echo form_password(array('name' => 'password', 'placeholder' => 'Wachtwoord', 'autocomplete' => 'off'));
                                echo form_submit(array('name' => 'login', 'class' => 'button', 'value' => 'Login'));
                                echo anchor('register', 'Registreer', array('id' => 'register', 'class' => 'linkbutton'));

                                echo form_close();
                                ?>
                        </section>
                    </div>
                </div>
            </div>
        </div><script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
</script><script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1EiLMvQPfwTRpR06fQhGwi1B4TjuKbFE&amp;sensor=true">
</script><script type="text/javascript" src="/assets/js/location-ck.js">
</script><script type='text/javascript' src='/assets/js/waypoints-0.0.1.js'>
</script><script type="text/javascript" src="/assets/js/script-ck.js">
</script><script type="text/javascript" src="/assets/js/add2home.js" >
</script><script type="text/javascript">
Waypoints
        .debug(true)
        .intercept('a')
        .ignore('.external')
        $(document).on('click', 'a.dynamic', function(e) {
        var href = $(e.target).attr('href');
        if (href === '#back') Waypoints.back();
        return false;
        });

        </script>
    </body>
</html>
