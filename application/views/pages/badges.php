<div id="container" class="absolute">
    <div id="toolbar" class="fixed">
        <span>Cultuvator <a href="#" id="open-left" class="left"><i class="icon-menu"></i></a><a href="logout" id="logout" class="right">Logout</a></span>
    </div>
    <div id="content">
        <section id="main_content" class="scrollable">
            <?php
                      $currentid = 0;
                      $headingprinted = false;
                     ?>
            <div class="article badgeInfo badges">
                <?php
                  //Als er badges zijn, deze per categorie tonen
                  if(!empty($badges)){
                    foreach ($badges as $badge) {
                    if($currentid == 0 || $badge['catid'] == $currentid){

                      if(!$headingprinted){
                        echo "<p class='title'>" . ucfirst($badge['catname']) . "</p>";
                        $headingprinted = true;
                      }

                      $currentid = $badge['catid'];

                      $baseurl = base_url();
                      echo "<span class='badge'><img src='".$baseurl."assets/img/badges/".$badge['catname']."_".$badge['iconUrl'].".png' /><p>" . $badge['title'] . "</p></span>";


                      }
                      else
                      {
                        $currentid = $badge['catid'];
                        echo "</div><div class='article badgeInfo badges'>";

                        echo "<p class='title'>" . ucfirst($badge['catname']) . "</p>";

                        $baseurl = base_url();
                        echo "<span class='badge'><img src='".$baseurl."assets/img/badges/".$badge['catname']."_".$badge['iconUrl'].".png' /><p>" . $badge['title'] . "</p></span>";


                      }
                    }
                  }
                  else
                  {
                        echo "<p class='title'>Geen badges gevonden</p>";
                        echo "<span class='title'><p>Bezoek meer culturele evenementen om badges te verdienen!</p></span>";
                  }

                  ?>
            </div>
        </section>
    </div>
</div>
