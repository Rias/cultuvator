<!-- Header op elke pagina -->
<!DOCTYPE html>
<?php
 if($this->session->userdata('logged_in') === null || !$this->session->userdata('logged_in')){
  redirect('login');
}
?><!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en" itemscope="" itemtype="http://schema.org/Article">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8"><!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=edge" /><![endif]-->
        <meta name="viewport" content="initial-scale=1,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <meta itemprop="name" content="Cultuvator">
        <meta itemprop="description" content="Share your culture activity">
        <meta itemprop="image" content="http://cultuvator.webduivel.be/assets/img/logo/512.png">

        <title>
            Cultuvator
        </title>
        <link rel="stylesheet" href="/assets/css/normalize.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="/assets/css/fontello.css">

    </head>
    <body>
        <div class="relative tall" id="wrapper">
            <div class="drawers absolute">
                <div class="left-drawer">
                    <ul id="nav">
                        <?php
                          $url = explode("/", current_url());
                          $page = end($url);

                          switch ($page) {
                            case 'badges':
                              $active = 'badges';
                              break;

                            case 'checkin':
                              $active = 'checkin';
                              break;

                            case 'eventdetail':
                              $active = 'checkin';
                              break;

                            case 'eventsearch':
                              $active = 'checkin';
                              break;

                            case 'home':
                              $active = 'home';
                              break;

                            case 'listevents':
                              $active = 'checkin';
                              break;

                            case 'share':
                              $active = 'checkin';
                              break;

                            default:
                              $active = 'home';
                              break;
                          }
                        ?>
                          <a href="home"><li class="icon-home <?php if($active === 'home'){ echo "active";}?>"></li></a> <!-- Home -->
                          <a href="listevents"><li class="icon-location <?php if($active === 'checkin'){ echo "active";}?>"></li></a> <!-- Checkin -->
                          <a href="badges"><li class="icon-award-1 <?php if($active === 'badges'){ echo "active";}?>"></li></a> <!-- Badges -->
                    </ul>
                </div>
            </div>
        </div>
