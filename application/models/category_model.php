<?php

class Category_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    //Id van een categorie opvragen op basis van de naam
    function get_id($catname){

        $this->db->select('id');
        $this->db->from('Category');
        $this->db->where('name', $catname);
        $this->db->limit(1);

        $query = $this->db->get();
        $result = $query->result();

        if($query -> num_rows() == 1)
        {
            foreach($result as $row)
            {
              return $row->id;
            }
        }

    }

    //Aantal punten van een gebruiker ophalen die deze heeft verdient in een categorie
    function get_points($catid, $userid){

        $this->db->select('points');
        $this->db->from('CategoryPoints');
        $this->db->where('userID', $userid);
        $this->db->where('categoryID', $catid);
        $this->db->limit(1);

        $query = $this->db->get();
        $result = $query->result();

        if($query -> num_rows() == 1)
        {
            foreach($result as $row)
            {
              return $row->points;
            }
        }
        else
        {
            return 0;
        }


    }

    //Als de gebruiker nog geen punten heeft deze toevoegen, anders deze aanpassen
    function set_points($catid, $userid, $points){

        $query = "INSERT INTO CategoryPoints (categoryID, userID, points) VALUES ($catid, $userid, 1) ON DUPLICATE KEY UPDATE points=$points";
        $this->db->query($query);

    }

    //Categorienaam bepalen op basis van heading id in cultuur databank
    function getCategoryName($cid){
        switch ($cid) {
            case $cid >= 1 && $cid < 5:
              return "evenementen";
              break;
            case 5:
              return "films";
              break;
            case $cid >=7 && $cid < 14:
              return "expo";
              break;
            case $cid >=14 && $cid < 22:
              return "muziek";
              break;
            case $cid >=22 && $cid < 25:
              return "podium";
              break;
            case $cid >=25 && $cid < 31:
              return "cursussen";
              break;
            case 31:
              return "sport";
              break;
            case 32:
              return "uitstappen";
              break;
            case 33:
              return "uitgaan";
              break;
            case 34:
              return "uit";
              break;
            case 35:
              return "varia";
              break;
            default:
              return "varia";
              break;

          }
    }

}

?>
