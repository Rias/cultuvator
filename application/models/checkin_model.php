<?php

class Checkin_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    //Een checkin toevoegen aan de databank
    function do_checkin($eid, $userid, $catid, $title){

        try {
            $data = array(
               'eventID' => $eid,
               'userID' => $userid,
               'date' => date("Y-m-d H:i:s"),
               'categoryID' => $catid,
               'title' => $title
            );

            $this->db->insert('Checkin', $data);

            return "success";
        } catch (Exception $e) {
            return $e->getMessage();
        }


    }

    //Alle checkins van een gebruiker ophalen
    function get_all($userid)
    {
        $this->db->select('eventID, categoryID, date, title');
        $this->db->from('Checkin');
        $this->db->where('userID', $userid);
        $this->db->order_by('date', 'desc');
        $this->db->limit(10);

        $query = $this->db->get();

        if($query -> num_rows() > 1)
        {
         return $query->result();
        }
        else
        {
         return false;
        }

    }
}

?>
