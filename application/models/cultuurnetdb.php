<?php

class Cultuurnetdb extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    //Lijst van evenementen opvragen
    function selectEvents($data){
    	$querystring = "http://build.uitdatabank.be/api/events/search?key=AEBA59E1-F80E-4EE2-AE7E-CEDD6A589CA9&format=json&datetype=today";

        //Elke key en value in de array toevoegen aan de url bv: array('city' => 'antwerpen')
    	foreach ($data as $d => $v) {
    		$querystring .= "&".$d."=".$v;
    	}

    	$json = file_get_contents($querystring);
		return json_decode($json, TRUE);
    }

    //Details van een evenement opvragen met behulp van de cdbid.
    function eventDetails($data){
        $querystring = "http://build.uitdatabank.be/api/event/".$data['cdbid']."?key=AEBA59E1-F80E-4EE2-AE7E-CEDD6A589CA9&format=json";
        return json_decode(file_get_contents($querystring));
    }
}

?>
