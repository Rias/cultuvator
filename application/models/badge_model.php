<?php

class Badge_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    //Toevoegen van een badge aan een user
    function earn_badge($catid, $type, $userid){

        //get badge id
        $this->db->select('badgeID');
        $this->db->from('Badge');
        $this->db->where('categoryID', $catid);
        $this->db->where('iconUrl', $type);
        $this->db->limit(1);

        $query = $this->db->get();
        $result = $query->result();

        if($query -> num_rows() == 1)
        {
            foreach($result as $row)
            {
              $badgeID = $row->badgeID;
            }
        }

        //user earns badge
        $data = array(
           'userID' => $userid,
           'badgeID' => $badgeID,
           'categoryID' => $catid
        );

        $this->db->insert('UserEarnBadge', $data);

    }

    //De titel en de url van een badge ophalen op basis van de user en categorie
    function get_badge($catid, $userid){

        //Badge id ophalen
        $this->db->select('badgeID');
        $this->db->from('UserEarnBadge');
        $this->db->where('categoryID', $catid);
        $this->db->where('userID', $userid);
        $this->db->order_by("badgeID", "desc");
        $this->db->limit(1);

        $query = $this->db->get();
        $result = $query->result();
        $badgeID = 0;

        if($query -> num_rows() == 1)
        {
            foreach($result as $row)
            {
              $badgeID = $row->badgeID;
            }
        }

        //Titel en url van badge ophalen op basis van badge id
        $this->db->select('title, iconUrl');
        $this->db->from('Badge');
        $this->db->where('badgeID', $badgeID);
        $this->db->limit(1);

        $query = $this->db->get();
        $result = $query->result();

        if($query -> num_rows() == 1)
        {
            foreach($result as $row)
            {
              $title = $row->title;
              $iconUrl = $row->iconUrl;
            }

            return array('title' => $title, 'iconUrl' => $iconUrl);
        }else
        {
            return 0;
        }

    }

    //Alle badges van een gebruiker ophalen
    function get_all($userid){

        $this->db->select('badgeID, categoryID');
        $this->db->from('UserEarnBadge');
        $this->db->where('userID', $userid);
        $this->db->order_by('categoryID');

        $query = $this->db->get();
        $result = $query->result();
        $data = array();

        //Voor elke badge de url en titel ophalen
        foreach($result as $row)
        {
            $badgeid = $row->badgeID;
            $categoryid = $row->categoryID;

            $this->db->select('iconUrl, title');
            $this->db->from('Badge');
            $this->db->where('badgeID', $badgeid);
            $this->db->limit(1);

            $q = $this->db->get();
            $r = $q->result();

            //Voor elke badge de categorienaam ophalen
            if($q -> num_rows() == 1)
            {
                foreach($r as $badge)
                {
                  $title = $badge->title;
                  $iconUrl = $badge->iconUrl;
                }

                //Categorienaam

                $this->db->select('name');
                $this->db->from('Category');
                $this->db->where('id', $categoryid);
                $this->db->limit(1);

                $catquery = $this->db->get();
                $catresult = $catquery->result();

                if($catquery->num_rows() == 1){
                    foreach($catresult as $cat)
                    {
                      $categoryname = $cat->name;
                    }
                }


                $data[] = array('title' => $title, 'iconUrl' => $iconUrl, 'catname' => $categoryname, 'catid' => $categoryid);

            }
        }

        return $data;
    }
}

?>
