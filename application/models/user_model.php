<?php

class User_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    //Functie om login te controleren
    public function checkLogin($username, $password){
        $this->load->helper('security'); //voor do_hash()
        $this->db->select('userId, username, password');
        $this->db->from('User');
        $this->db->where('username', $username);
        $this->db->where('password', do_hash($password));
        $this->db->limit(1);

        $query = $this->db->get();

        if($query -> num_rows() == 1)
        {
         return $query->result();
        }
        else
        {
         return false;
        }
    }

    //Registreren van een user array $data met username, name, surname, email en password
    public function registerUser($data){
        $this->load->helper('security');

        $data = array(
               'username' => $data["username"],
               'name' => $data["name"],
               'surname' => $data["surname"],
               'email' => $data["email"],
               'password' => do_hash($data["password"])
        );

        $this->db->insert('User', $data);

        return true;
    }

    //Functie om de naam van de gebruiker te verkrijgen via de userid
    public function get_name($userid)
    {
        $this->db->select('name, surname');
        $this->db->from('User');
        $this->db->where('userid', $userid);
        $this->db->limit(1);

        $query = $this->db->get();

        if($query -> num_rows() == 1)
        {
         return $query->result();
        }
        else
        {
         return false;
        }
    }
}

?>
