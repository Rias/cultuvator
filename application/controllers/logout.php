<?php
class Logout extends CI_Controller {

    //Loginsessie leegmaken en gebruiker naar home sturen
    public function index()
    {
        $this->session->set_userdata('logged_in', false);
        $this->session->sess_destroy();
        redirect('home','refresh');
    }
}

?>
