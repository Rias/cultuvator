<?php

class Pages extends CI_Controller {

	public function view($page = 'login')
	{
		if ( ! file_exists('application/views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data = "";

    //Als de opgevraagde pagina login, register of registersuccess de header en footer niet tonen
		if($page == 'login' || $page == 'register' || $page == 'registersuccess'){
			$this->load->view('pages/'.$page);
		}
		else{

      //Kijken welke pagina het is
			switch ($page) {
				case 'home':
					$this->load->model('user_model');
          $this->load->model('checkin_model');
          $this->load->model('category_model');

          $loggedin = $this->session->userdata('logged_in');
          $userid = $loggedin['id'];

          $checkins = array();

          //Get user first & last name
          $userFullName = $this->user_model->get_name($userid);

					//Get user last checkins
					$query = $this->checkin_model->get_all($userid);

          if(!empty($query)){
            foreach ($query as $checkin) {
            $catName = $this->category_model->getCategoryName($checkin->categoryID);

            $checkins[] = array('title' => $checkin->title, 'category' => $catName, 'date' => $checkin->date, 'eventID' => $checkin->eventID);
            }
          }
          else
          {
            $checkins = false;
          }


          $data = array("checkins" => $checkins, 'userFullName' => $userFullName[0]);

					break;

				case 'listevents':
					$this->load->model('cultuurnetdb');
					$latlng = $this->session->userdata('latitude') . ";" . $this->session->userdata('longitude') . "!5km";

          //Als er gepost is de query uitvoeren en de zoekresultaten weergeven
          $q = $this->input->post('q');
          if(isset($q) && !empty($q))
          {
            $jsondata = $this->cultuurnetdb->selectEvents(array('q' => str_replace(' ', '%20', $this->input->post('q')), 'latlng' => $latlng, 'sort' => 'proximity'));
          }
          //Anders alle dichtbijzijnste evenementen ophalen
          else
          {
            $jsondata = $this->cultuurnetdb->selectEvents(array('latlng' => $latlng, 'sort' => 'proximity', 'pagelength' => '20'));
          }


					$data = array('jsondata' => $jsondata);
				break;

				case 'eventdetail':
					$this->load->model('cultuurnetdb');

          //Evenement details opvragen
					$jsondata = $this->cultuurnetdb->eventDetails(array('cdbid' => $this->input->get('id')));
					$data = array('event' => $jsondata);
				break;

				case 'checkin':
          $this->load->model('checkin_model');
          $this->load->model('category_model');

          //Evenementid en categorie uit GET halen
          $eventid = $this->input->get('eid');
          $catid = $this->input->get('cid');
          $catName = $this->category_model->getCategoryName($catid);
          $eventTitle = htmlspecialchars($this->input->get('title'));


          $loggedin = $this->session->userdata('logged_in');
                  $userid = $loggedin['id'];

                  //Als de checkin successvol is gelukt
                  if($this->checkin_model->do_checkin($eventid, $userid, $catid, $eventTitle) == "success"){
                    $this->load->model('badge_model');

                    $categoryId = $this->category_model->get_id($catName);
                    $currentPoints = $this->category_model->get_points($categoryId, $userid);
                    $currentPoints += 1;
                        $neededPoints = 0;
                        $totalPoints = 0;
                        $badgeEarned = 0;

                    //Kijken naar huidig aantal punten en bepalen of er een badge moet uitgereikt worden
                    if($currentPoints == 25){
                      $this->badge_model->earn_badge($categoryId, 1, $userid); //1 = bronze
                      $badgeEarned = true;
                    }
                    else if($currentPoints == 50){
                      $this->badge_model->earn_badge($categoryId, 2, $userid); //2 = silver
                      $badgeEarned = true;
                    }
              			else if($currentPoints == 75){
              				$this->badge_model->earn_badge($categoryId, 3, $userid); //3 = gold
                      $badgeEarned = true;
              			}

                      //Totaal aantal punten berekenen
                        if($currentPoints < 25){
                            $neededPoints = 25 - $currentPoints;
                            $totalPoints = 25;
                        }
                        else if ($currentPoints < 50) {
                            $neededPoints = 50 - $currentPoints;
                            $totalPoints = 50;
                        }
                        else if($currentPoints < 75)
                        {
                            $neededPoints = 75 - $currentPoints;
                            $totalPoints = 75;
                        }

              			$this->category_model->set_points($categoryId, $userid, $currentPoints);

                    //Als er geen badge gevonden is, een default afbeelding meegeven
              			if($this->badge_model->get_badge($categoryId, $userid) == 0){
              				$badgeFound = false;

                            $data = array('checkin' => "success", 'badgeFound' => $badgeFound, 'eventTitle' => $eventTitle, 'neededPoints' => $neededPoints, 'totalPoints' => $totalPoints);
              			}

                    //Anders de url van de badge meegeven om deze te tonen in de view
              			else
              			{
              				$badgeFound = true;
              				$badgeData = $this->badge_model->get_badge($categoryId, $userid);
              				$badgeTitle = $badgeData['title'];
              				$badgeIconUrl = $badgeData['iconUrl'];

                            $data = array('checkin' => "success", 'badgeFound' => $badgeFound, 'badgeTitle' => $badgeTitle, 'badgeIconUrl' => $badgeIconUrl, 'eventTitle' => $eventTitle, 'neededPoints' => $neededPoints, 'catname' => $catName, 'totalPoints' => $totalPoints);
              			}


              		}
              		else
              		{
              			$data = array('checkin' => $this->checkin_model->do_checkin($eventid, $userid));
              		}

					break;

          case 'share':
              //Evenement titel uit GET halen en meegeven.
              $eventTitle = $this->input->get('title');
              $data = array('title' => $eventTitle);
          break;

          case 'badges':
              $loggedin = $this->session->userdata('logged_in');
              $userid = $loggedin['id'];

              //Alle badges van user ophalen
              $this->load->model("badge_model");

              //Meegeven in een array
              $data = array('badges' => $this->badge_model->get_all($userid));


          break;
			}

      //Header, footer en pagina tonen.
			$this->load->view('templates/header');
			$this->load->view('pages/'.$page, $data);
			$this->load->view('templates/footer');
		}



	}

}
