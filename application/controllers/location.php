<?php
class Location extends CI_Controller {

	public function index()
	{
		//Locatiedata in sessie zetten
		$this->session->set_userdata('latitude', $this->input->post('latitude'));
		$this->session->set_userdata('longitude', $this->input->post('longitude'));

		$feedback['status'] = "success";
		header('Content-type: application/json');
		echo json_encode($feedback);

	}
}

?>
