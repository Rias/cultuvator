<?php

class User extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function register(){
        $this->load->library('form_validation');

        //Validation regels
        $this->form_validation->set_rules('username', 'gebruikersnaam', 'required|is_unique[User.username]');
        $this->form_validation->set_message('is_unique[User.username]', 'Gebruikersnaam is al in gebruik.');
        $this->form_validation->set_rules('name', 'voornaam', 'required');
        $this->form_validation->set_rules('surname', 'achternaam', 'required');
        $this->form_validation->set_rules('email', 'e-mail adres', 'required|valid_email');
        $this->form_validation->set_rules('password', 'wachtwoord', 'required');
        $this->form_validation->set_rules('password2', 'herhaal wachtwoord veld', 'required|matches[password]');


        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('pages/register');
        }
        else
        {
            if($this->User_model->registerUser($this->input->post())){
                redirect('registersuccess');
            }
        }


    }

    //Gebruiker proberen in te loggen
    public function login(){
    	$this->load->library('form_validation');

       $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
       $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

       if($this->form_validation->run() == FALSE)
       {
         //Validatie mislukt, terug naar login
         $this->load->view('pages/login');
       }
       else
       {
         //Ga naar homepage
         redirect('home', 'refresh');
       }
	}

    //Database controleren op username en wachtwoord
    function check_database($password)
     {
       //Field validation succeeded.  Validate against database
       $username = $this->input->post('username');

       //query the database
       $result = $this->User_model->checkLogin($username, $password);

       if($result)
       {
         $sess_array = array();
         foreach($result as $row)
         {
           $sess_array = array(
             'id' => $row->userId,
             'username' => $row->username
           );
           $this->session->set_userdata('logged_in', $sess_array);
         }
         return TRUE;
       }
       else
       {
         $this->form_validation->set_message('check_database', 'Foute gebruikersnaam of wachtwoord');
         return false;
       }
     }


}



?>
