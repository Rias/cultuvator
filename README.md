Cultuvator
==========

Project Cultuurnet - Thomas More

This is a PHP web application making use of CodeIgniter, LESS and geolocation to teach myself some more about
these things while still getting the required parts for the assignment.

This is also the first project I completely develop while using Github.





Credits
========
The amazing snap menu is created with snap.js developed by <a href="https://github.com/jakiestfu">Jakiestfu</a>.
